#! xtclsh

set myProject "papi"
set myScript "papi"
 

proc show_help {} {

   global myScript

   puts "usage: $myScript <options>"
   puts ""
   puts "options:"
   puts "   init              - Create a project file."
   puts "   synth             - Synthesize the project."
   puts "   show_help         - print this message"
   puts ""
}

proc synth {} {
	project open [glob *.xise]
	process run "Generate Programming File"
	puts "The synthesized bit and bin files can be found in the ise_work directory"
}

proc init_lib {} {
    cd ../..
	init
}

proc init {} {
   set projectName [file tail [pwd]]

   puts $projectName

	if { ! [ file exists $projectName.xise ] } { 
	   ## project file isn't there, create it.
		project new $projectName.xise
	} else {
		project open [glob $projectName.xise]
	}
	project set family spartan6
	project set device xc6slx9
	project set package tqg144
	project set speed -2
	project set "Generate Detailed MAP Report" true
	project set "Allow unmatched LOC Constraints" true
	project set "Create Binary Configuration File" true
	project set "Allow unmatched LOC Constraints" true
	project set "Create Binary Configuration File" true
	project set "Working Directory" ise_work
	project set "Simulation Run Time ISim" 5ms
	xfile remove [ search *.ucf -type file ]
	set hdl_files [glob *.vhd* node_modules/*/*.vhd* node_modules/*/*.ucf]
	foreach item $hdl_files {
		puts $item
		xfile add $item
	}
	project close
}

proc main {} {

   if { [llength $::argv] == 0 } {
      show_help
      return true
   }

   foreach option $::argv {
      switch $option {
         "init"                { init }
		 "init_lib"            { init_lib }
         "synth"               { synth }
         default               { puts "unrecognized option: $option"; show_help }
      }
   }
}

if { $tcl_interactive } {
   show_help
} else {
   if {[catch {main} result]} {
      puts "$myScript failed: $result."
   }
}