#papilio

## Description

Tool to build Papilio FPGA projects

## Usage

To install papilio from npm, run:

```
$ npm install -g papilio
```

```node ./bin/papilio --help```

## License

Copyright (c) 2017 Jack Gassett

[MIT License](http://en.wikipedia.org/wiki/MIT_License)
