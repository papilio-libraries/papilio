/* synth commander component
 * To use add require('../cmds/synth.js')(program) to your commander.js based node executable before program.parse
 */
'use strict';

module.exports = function(program) {

	program
		.command('synth')
		.version('0.0.0')
		.description('Synthesize the Papilio project and generate a bit/bin file.')
		.action(function (/* Args here */) {
            //This example is from https://nodejs.org/api/child_process.html

            const { spawn } = require('child_process');
            const command = spawn('xtclsh', ['papi.tcl', 'synth']);

            command.stdout.on('data', (data) => {
                console.log(`${data}`);
            });

            command.stderr.on('data', (data) => {
                console.log(`${data}`);
            });

            command.on('close', (code) => {
                console.log(`child process exited with code ${code}`);
            });

		});

};
