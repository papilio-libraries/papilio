/* init commander component
 * To use add require('../cmds/init.js')(program) to your commander.js based node executable before program.parse
 */
'use strict';

module.exports = function(program) {

	program
		.command('init')
		.version('0.0.0')
		.description('Init a Papilio project and create the Xilinx ISE file.')
		.action(function (/* Args here */) {
			//This example is from https://stackoverflow.com/questions/20643470/execute-a-command-line-binary-with-node-js
            const { exec } = require('child_process');
            exec('xtclsh papi.tcl init', (err, stdout, stderr) => {
                if (err) {
                    // node couldn't execute the command
                    return;
                }

                // the *entire* stdout and stderr (buffered)
                console.log(`stdout: ${stdout}`);
                console.log(`stderr: ${stderr}`);
            });
		});

};
